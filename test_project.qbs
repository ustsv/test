import qbs.base
import qbs.FileInfo
import qbs.ModUtils
import qbs.Utilities
import qbs.TextFile
import qbs.Probes
import qbs.Environment

Project {
    name: 'test_project'
    minimumQbsVersion: '1.11.0'

    Stm32Application {
        name: 'main'
        version: '0.2.0'
        targetName: 'main'
        type: [ 'application', 'hex', 'bin', 'output', 'map', 'size' ]

        cpp.useCPrecompiledHeader: true

        cpp.defines: [
            'STM32F100xB',
            'USE_HAL_DRIVER'
        ]

        property string STM32Cube_FW_F1: Environment.getEnv('USERPROFILE') + '/STM32Cube/Repository/STM32Cube_FW_F1_V1.7.0/'

        cpp.includePaths: [
            'Inc',
            STM32Cube_FW_F1 + 'Drivers/STM32F1xx_HAL_Driver',
            STM32Cube_FW_F1 + 'Drivers/STM32F1xx_HAL_Driver/Inc',
            STM32Cube_FW_F1 + 'Drivers/CMSIS/Include',
            STM32Cube_FW_F1 + 'Drivers/CMSIS/Device/ST/STM32F1xx/Include'
        ]

        Group {
            name: 'STM32F1xx_HAL_Driver'
            overrideTags: false
            fileTags: ['src']
            prefix:  STM32Cube_FW_F1 + 'Drivers/STM32F1xx_HAL_Driver/Src/'
            files: [
                'stm32f1xx_hal_gpio_ex.c',
                'stm32f1xx_hal_crc.c',
                'stm32f1xx_hal_tim.c',
                'stm32f1xx_hal_tim_ex.c',
                'stm32f1xx_hal.c',
                'stm32f1xx_hal_rcc.c',
                'stm32f1xx_hal_rcc_ex.c',
                'stm32f1xx_hal_gpio.c',
                'stm32f1xx_hal_dma.c',
                'stm32f1xx_hal_cortex.c',
                'stm32f1xx_hal_pwr.c',
                'stm32f1xx_hal_flash.c',
                'stm32f1xx_hal_flash_ex.c',
                'stm32f1xx_hal_uart.c'
            ]
        }

        Group {
            name: 'Src'
            overrideTags: false
            fileTags: ['src']
            prefix: '**/'
            files: [
                '*.c', '*.cpp', '*.h' , '*.hpp'
            ]

            excludeFiles: [
                'c_pch_src.hpp'
            ]
        }

        Group {
            condition: cpp.useCPrecompiledHeader
            name: 'PrecompiledHeader'
            files: ['c_pch_src.hpp']
            fileTags: ['c_pch_src']
        }

        Group {
            name: 'Linkerscript'
            overrideTags: false
            fileTags: [ '_linkerscript' ]
            prefix: ''
            files: [
                'STM32F100RBTx_FLASH.ld'
            ]
        }

        Group {
            condition: qbs.toolchain.contains('gcc');
            name: 'Asm'
            overrideTags: false
            fileTags: ['asm', 'src']
//            prefix: STM32Cube_FW_F1 + 'Drivers/CMSIS/Device/ST/STM32F1xx/Source/Templates/gcc/'
            prefix: ''
            files: [
                'startup_stm32f100xb.s'
            ]
        }

        Group {
//            name: 'The App itself'
            fileTagsFilter: ['application', 'hex', 'bin']
            qbs.install: true
            qbs.installDir: 'bin'
        }

        Group {
//            name: 'LinkerMap'
            overrideTags: false
            fileTagsFilter: ['map']
            qbs.install: true
            qbs.installDir: 'map'
        }

        Group {
            name: 'CubeMX'
            files: [ '*.ioc']
            overrideTags: false
            fileTags: ['src']
            qbs.install: true
            qbs.installDir: 'src'
        }

        Group {
            name: 'Git'
            files: [
                '.gitignore',
                '.git/config',
                '.git/description'
            ]
        }

        Group {
            name: 'Task'
            files: [ '*.tasks' ]
        }
    }

    Stm32Document {
        name: 'document'
        Depends { name: 'main' }
        Depends {
            name: 'doxygen'
            versionAtLeast: '0.1.0'
        }
        Depends { name: 'Qt';
            submodules: [ 'core' ]
            required: false
        }
        builtByDefault: false
        type: [ 'qdoc-output', 'doxygen_out', 'doxygen_out2', 'txt_output' ]

        Group {
            name: 'Doxygen_cfg'
            files: [ 'Doxyfile' ]
        }

        Group {
            name: 'Doxygen_out'
            //            prefix: Utilities.getHash(destinationDirectory.baseDir) + '/html/*'
            //            prefix: buildDirectory + '/'
            fileTagsFilter: ['doxygen_out']
            qbs.install: true
            qbs.installDir: 'doc'
            qbs.installSourceBase: product.buildDirectory
        }

//        Group {
//            name: 'Doxygen_out2'
//            files: [ product.buildDirectory + '/html/*.*']
//            qbs.install: true
//            qbs.installDir: 'doc'
//        }

        Group {
            name: 'Doxygen_in'
            files: [ '*.md']
            fileTags: [ 'txt_input' ]
        }

        Group {
            name: 'Doc'
            fileTagsFilter: [ 'txt_input', 'txt_output', 'doxygen_cfg' ]
            qbs.install: true
            qbs.installDir: 'doc'
        }
    }

    InstallPackage {
        name: 'archive'
        Depends { name: 'archiver' }
        Depends { name: 'main' }
        Depends {
            name: 'document'
            required: true
        }
        builtByDefault: false
        archiver.type: '7zip'
        archiver.compressionLevel: '8'
        archiver.archiveBaseName: product.targetName
        archiver.outputDirectory: product.destinationDirectory
    }
}

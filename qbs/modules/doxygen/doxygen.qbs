import qbs
import qbs.Environment
import qbs.File
import qbs.FileInfo
import qbs.Probes
import qbs.ModUtils
import qbs.Utilities
import qbs.WindowsUtils
import qbs.Xml
import qbs.TextFile
import 'utils.js' as utils

Module {
    name:   'doxygen'
    version: '0.1.1'
    additionalProductTypes:   ['qdoc-output', 'doxygen_out', 'doxygen_out2', 'doxygen_tmp']

    validate: {
//        console.info('Module.validate: ' + name);
    }

    property string qdocOutputDir: product.buildDirectory + '/html'
    property stringList qdocEnvironment

    Probes.BinaryProbe {
        id: doxygenProbe;
        names: ['doxygen'];
        platformPaths: {
            var paths = base
            var env32 = Environment.getEnv('PROGRAMFILES')
            var env64 = Environment.getEnv('ProgramW6432')
            var msys2 = Environment.getEnv('MSYS2')
            var scoop = Environment.getEnv('SCOOP')
            var scoop_global = Environment.getEnv('SCOOP_GLOBAL')
            paths.push(FileInfo.joinPaths(env32, '/doxygen'))
            paths.push(FileInfo.joinPaths(env64, '/doxygen'))
            paths.push(FileInfo.joinPaths(scoop, '/apps/doxygen/current'))
            paths.push(FileInfo.joinPaths(scoop_global, '/apps/doxygen/current'))
            paths.push(FileInfo.joinPaths(msys2, '/usr/bin'))
            paths.push(FileInfo.joinPaths(msys2, '/mingw32/bin'))
            paths.push(FileInfo.joinPaths(msys2, '/mingw64/bin'))
            return paths
        }
    }

    readonly property string doxygenPath: {
//        console.info(doxygenProbe.filePath);
        if (doxygenProbe.found)
            return doxygenProbe.filePath;
        throw 'Doxygen not found with Probe';
        return undefined;
    }

    Probes.BinaryProbe {
        id: dotProbe;
        names: ['dot'];
        platformPaths: {
            var paths = base
            var env32 = Environment.getEnv('PROGRAMFILES')
            var env64 = Environment.getEnv('ProgramW6432')
            var msys2 = Environment.getEnv('MSYS2')
            var scoop = Environment.getEnv('SCOOP')
            var scoop_global = Environment.getEnv('SCOOP_GLOBAL')
            paths.push(FileInfo.joinPaths(env32, '/graphviz'))
            paths.push(FileInfo.joinPaths(env64, '/graphviz'))
            paths.push(FileInfo.joinPaths(scoop, '/apps/graphviz/current'))
            paths.push(FileInfo.joinPaths(scoop_global, '/apps/graphviz/current'))
            paths.push(FileInfo.joinPaths(msys2, '/usr/bin'))
            paths.push(FileInfo.joinPaths(msys2, '/mingw32/bin'))
            paths.push(FileInfo.joinPaths(msys2, '/mingw64/bin'))
            return paths
        }
    }

    readonly property string dotPath: {
//        console.info(dotProbe.filePath);
        if (dotProbe.found)
            return dotProbe.filePath;
        throw 'Dot not found with Probe';
        return undefined;
    }

    FileTagger {
        patterns: ['Doxyfile', 'doxyfile'];
        fileTags: ['doxygen_cfg'];
    }

//    Scanner {
//        id: scan;
//        condition: true;
//        inputs: 'doxygen_cfg';
//        scan: {
//            console.info('Scanner');
//            xml = XmlDomDocument(input.fileName);
//            dependencies = [];
//            // do something with the xml
//            return dependencies;
//        }
//        searchPaths: {
//            return;
//        }
//    }

    //    Rule {
    //        id: doxygenInit;
    //        condition:  doxygenProbe.found;
    //        alwaysRun:  true;
    //        multiplex:  true;
    //        inputs: ['doxygen_cfg'];
    //        Artifact {
    //            fileTags: ['doxygen_init'];
    //            filePath:  input.baseDir + 'Doxyfile';
    //            alwaysUpdated: true;
    //        }
    //        prepare: {
    //            var doxygen = ModUtils.moduleProperty(product, 'doxygenPath');
    //            var args = ['-g', input.filePath];
    //            var cmd = new Command(doxygen, args);
    //            cmd.description = 'Doxygen init' + ' -g ' + input.filePath;
    //            cmd.extendedDescription = 'Doxygen init process ...'
    //            cmd.highlight = 'doxygen';
    //            cmd.silent = false;
    //            cmd.arguments;
    //            cmd.environment;
    //            cmd.maxExitCode;
    //            cmd.program;
    //            cmd.responseFileThreshold;
    //            cmd.responseFileArgumentIndex;
    //            cmd.responseFileUsagePrefix;
    //            cmd.stderrFilterFunction;
    //            cmd.stdoutFilterFunction;
    //            cmd.workingDirectory;
    //            cmd.stdoutFilePath;
    //            cmd.stderrFilePath;
    //            return cmd;
    //        }
    //    }

    Rule {
        id: doxygen
//        condition:  doxygenProbe.found // && dotProbe.found;
        alwaysRun:  true
        multiplex:  true
        explicitlyDependsOn: [ 'utils' ]
        inputs: [ 'doxygen_cfg' ]
        Artifact {
            fileTags: [ 'txt_output' ]
            filePath:  input.baseName + '.new'
        }

//        outputArtifacts: [ {
//                fileTags: 'doxygen_tmp',
//                filePath: ['doxygen_sqlite3.db']
//            }
//       ]
        //            //            {
        //            //                fileTags: 'doxygen_tmp',
        //            ////                filePath: 'GeneratedFiles/doxygen_sqlite3.db'
        //            //                filePath: 'doxygen_sqlite3.db'
        //            //            },
        //            {
        //                fileTags: 'doxygen_out',
        //                //                filePath: 'GeneratedFiles/html'
        //                filePath: 'html'
        //                //                filePath: utils.doxygenOutputArtifacts(product, inputs, input)
        //            }
        //        ]

//        outputFileTags: ModUtils.allFileTags(utils.qdocFileTaggers())
//        outputArtifacts: utils.outputArtifacts(product, input)

//        outputFileTags: [ 'doxygen_out', 'doxygen_tmp' ]

        prepare: {
            console.info('doxygen');
            var cmds = [];
            var doxygen = ModUtils.moduleProperty(product, 'doxygenPath');
            var cmdp = new JavaScriptCommand();
            var sourceDir = product.sourceDirectory;
            var workdir = product.buildDirectory;
            cmdp.workdir = workdir;
            cmdp.dot = ModUtils.moduleProperty(product, 'dotPath');
            cmdp.outfile =  workdir + '/' + input.baseName;
 //           cmdp.outfile = outfile;
            cmdp.description = 'Patching: ' + input.filePath + ' ' + cmdp.outfile;
            cmdp.highlight = 'filegen';
            cmdp.silent = false;
            cmdp.sourceCode = function() {
                var file = new TextFile(input.filePath, TextFile.ReadOnly);
                var content = file.readAll();
                file.close();
//                var reg = new RegExp();
                content = content.replace(/^#(.*)$/gm, '');
                content = content.replace(/^[ \t]*\n\r$/gm, '');
                content = content.replace(/^[ \t]*(OUTPUT_DIRECTORY)[ \t]*=.*$/gm, '$1=' + workdir);
                content = content.replace(/^[ \t]*(DOT_PATH)[ \t]*=.*$/gm, '$1=' + dot);
                content = content.replace(/^[ \t]*CLANG_ASSISTED_PARSING[ \t]*=.*$/gm, '');
                content = content.replace(/^[ \t]*CLANG_OPTIONS[ \t]*=.*$/gm, '');
                content = content.replace(/^[ \t]*LATEX_TIMESTAMP[ \t]*=.*$/gm, '');
                content = content.replace(/^[ \t]*WARN_AS_ERROR[ \t]*=.*$/gm, '');
                file = new TextFile(outfile, TextFile.WriteOnly);
                file.write(content);
                file.close();
            }
            cmds.push(cmdp);

            var args = [cmdp.outfile];
            var cmd = new Command(doxygen, args);
            cmd.description = '***\tdoxygen\t***\t' + cmdp.outfile;
            cmd.silent = false;
            cmd.arguments;
            cmd.environment;
            cmd.maxExitCode;
            cmd.program;
            cmd.responseFileThreshold;
            cmd.responseFileArgumentIndex;
            cmd.responseFileUsagePrefix;
            cmd.workingDirectory = sourceDir;
            cmd.stdoutFilePath;
            cmd.stderrFilePath;
            cmd.stdoutFilterFunction = function(output) {
                return '';
            };
            cmd.stderrFilterFunction = function(output) {
                return '';
            };
            cmds.push(cmd);

            return cmds;
        }
    }

    Rule {
        id: doxygen_post;
        condition: true;
        multiplex: true;
        //        alwaysRun:  true;
        inputs: [ 'doxygen_out'];
        Artifact {
            fileTags:   ['doxygen_out2'];
            filePath:   ['doxygen.lst'];
        }

        //        doxygen_post.file: utils.dump_prop(ModUtils.moduleProperty(utils, 'file_log'), this);
        prepare: {
            var args = [];
            var cmd = new JavaScriptCommand();
            cmd.sourceCode = function() {
                var file = new TextFile(output.filePath, TextFile.WriteOnly);
                //                utils.dump_prop(file, inputs['doxygen_out']);
                file.close();
            }
            cmd.description = '***\tDoxygen Post Rule';
            cmd.silent = false;
            //            utils.dump_prop(file, this, 0);
            return cmd;
        }
    }
}

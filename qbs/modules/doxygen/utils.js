//var File = loadExtension("qbs.File");
//var FileInfo = loadExtension("qbs.FileInfo");
var ModUtils = require("qbs.ModUtils");
//var PathTools = loadExtension("qbs.PathTools");
//var Process = loadExtension("qbs.Process");
//var UnixUtils = loadExtension("qbs.UnixUtils");
//var Utilities = loadExtension("qbs.Utilities");
//var WindowsUtils = loadExtension("qbs.WindowsUtils");

var file_log;

function dump_prop(file, start, level) {
    if (level === undefined) level=0;
    var i = 0;
    var pref = "";
    for(x=0;x<level;x++)
        pref += ("\t");
    for(prop in start) {
        if (start.hasOwnProperty(prop)) {
            file.write(pref + level + "." + i + ":\t");
            file.write(prop + " [" + typeof start[prop] + "]\t=\t");
            switch (typeof start[prop]) {
            case 'object':
                file.write("\n");
                dump_prop(file, start[prop], level+1);
                break;
            case 'function':
                file.writeLine(pref + "[code]" );
                break;
            default:
                file.writeLine(pref + start[prop] );
            }
            i++;
        }
    }
}

var _qdocDefaultFileTag = "qdoc-output";

function qdocArgs(product, input, outputDir) {
    var args = [input.filePath];
    var qtVersion = ModUtils.moduleProperty(product, "versionMajor");
    if (qtVersion >= 5) {
        args.push("-outputdir");
        args.push(outputDir);
    }

    return args;
}

function qdocFileTaggers() {
    var t = _qdocDefaultFileTag;
    console.info("qdocFileTaggers");
    return {
        ".qhp": [t, "qhp"],
        ".qhp.sha1": [t, "qhp-sha1"],
        ".css": [t, "qdoc-css"],
        ".html": [t, "qdoc-html"],
        ".index": [t, "qdoc-index"],
        ".png": [t, "qdoc-png"]
    };
}

function outputArtifacts(product, input) {
    console.info("outputArtifacts");
    var tracker = new ModUtils.BlackboxOutputArtifactTracker();
    tracker.hostOS = product.moduleProperty("qbs", "hostOS");
    tracker.shellPath = product.moduleProperty("qbs", "shellPath");
    tracker.defaultFileTags = [_qdocDefaultFileTag];
    tracker.fileTaggers = qdocFileTaggers();
    tracker.command = "doxygen";
    tracker.commandArgsFunction = function (outputDirectory) {
        return [input.filePath];
    };
    tracker.commandEnvironmentFunction = function (outputDirectory) {
        return "";
    };
    //    ModUtils.dumpObject(tracker);
    return tracker.artifacts(ModUtils.moduleProperty(product, "qdocOutputDir"));
}

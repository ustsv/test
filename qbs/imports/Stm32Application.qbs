import qbs
import qbs.FileInfo
import qbs.Utilities
import qbs.TextFile

Stm32Product {

    Rule {
        id: bin
        condition: true
        multiplex: false
        alwaysRun: false
        inputs: ['application']
        Artifact {
            fileTags: ['bin']
            filePath: product.targetName + '.bin'
        }
        prepare: {
            var args = ['-O', 'binary', input.filePath, output.filePath];
            var cmd = new Command(product.cpp.objcopyPath, args);
            cmd.description = 'converting ' + input.filePath + ' to bin';
            cmd.silent = false;
            cmd.highlight = 'compiler';
            return [cmd];
        }
    }

    Rule {
        id: hex
        condition: true
        multiplex: false
        alwaysRun: false
        inputs: ['application']
        Artifact {
            fileTags: ['hex']
            filePath: product.targetName + '.hex'
        }
        prepare: {
            var args = ['-O', 'ihex', input.filePath, output.filePath];
            var cmd = new Command(product.cpp.objcopyPath, args);
            cmd.description = 'converting ' + input.filePath + ' to hex';
            cmd.silent = false;
            cmd.highlight = 'compiler';
            return [cmd];
        }
    }

    Rule {
        id: size
        condition: true
        multiplex: false
        alwaysRun: true
        inputs: ['application']
        Artifact {
            fileTags: ['size']
            filePath: FileInfo.joinPaths(qbs.nullDevice, 'size')
        }
        prepare: {
            var args = ['-B', '-d', input.filePath];
            var cmd = new Command(product.cpp.toolchainPrefix + 'size', args);
            cmd.description = 'size ' + input.filePath;
            cmd.silent = false;
            cmd.highlight = 'compiler';
            return [cmd];
        }
    }

    Rule {
        id: map
        condition: true
        multiplex: false
        alwaysRun: false
        inputs: [ 'application']
        Artifact {
            fileTags:   ['map']
            filePath:   product.targetName + '.map'
        }
        prepare: {
            var args = [];
            var cmd = new JavaScriptCommand();
            cmd.sourceCode = function() {}
            cmd.description = 'Fake Map Rule';
            cmd.silent = false;
            return [cmd];
        }
    }
}

import qbs
import qbs.FileInfo

Product {
    Depends { name: 'cpp' }

    property string compilerTest
    PropertyOptions {
        name: 'compilerTest'
        description: 'preprocessor macros that are defined when using this particular compiler'
    }

    Properties {
        condition: qbs.buildVariant == 'release'
        cpp.debugInformation: false
        cpp.defines: outer.concat('NDEBUG')
        cpp.optimization: 'all'
    }

    Properties {
        condition: qbs.buildVariant == 'debug'
        cpp.debugInformation: true
        cpp.defines: outer.concat('DEBUG')
        cpp.optimization: 'none'
    }

    consoleApplication: true

    cpp.positionIndependentCode: false
    cpp.executableSuffix: '.elf'
    cpp.separateDebugInformation: true
    cpp.debugInfoSuffix: '.debug'

    cpp.commonCompilerFlags: [
        '-Wno-unused',
        '-Og',
        '-fdata-sections',
        '-ffunction-sections',
//        '-flto',
//!!!        '-MMD',
//        '-MP',
//        '-Wa,-a,-ad,-alms' + destinationDirectory + './out.lst'
    ]

    cpp.linkerFlags: [
        '--gc-sections',
        '-lc',
        '-lm',
        '-lnosys',
        '-T' + FileInfo.joinPaths(sourceDirectory,'STM32F100RBTx_FLASH.ld'),
        '-Map=' + FileInfo.joinPaths(destinationDirectory,product.targetName + '.map'),
    ]

    cpp.driverFlags: [
        '-mcpu=cortex-m3',
        '-mthumb',
        '-specs=nano.specs',
        '-fuse-linker-plugin'
    ]

    cpp.cxxLanguageVersion: 'c++11'
    cpp.cLanguageVersion: 'gnu11'

    cpp.cxxFlags: [ ]

    cpp.cFlags: [ ]

/*
    cpp.assemblerName: 'gcc'
    cpp.assemblerFlags: [
        '-x',
        'assembler-with-cpp',
        '-mcpu=cortex-m3',
        '-mthumb',
        '-specs=nano.specs',
        '-DSTM32F100xB',
        '-DUSE_HAL_DRIVER',
//        '-DNDEBUG',
    ]
*/
    FileTagger {
        patterns: '*.map'
        fileTags: ['map']
    }
}

import qbs
import qbs.FileInfo
import qbs.TextFile

Product {

    Rule {
        id: text
        condition: true
        multiplex: false
        alwaysRun: false
        inputs: ['txt_input']
        Artifact {
            fileTags: [ 'txt_output' ]
//            filePath:  + Utilities.getHash(input.baseDir) + '/' + input.baseName + '.new'
            filePath:  input.baseName + '.new'
        }
        prepare: {
            var cmd = new JavaScriptCommand();
            cmd.description = 'Processing ' + input.filePath;
            cmd.highlight = 'codegen';
            cmd.silent = false;
            cmd.sourceCode = function() {
                var file = new TextFile(input.filePath);
                var content = file.readAll();
                file.close()
                content = content.replace(/\*/g, '#');
                file = new TextFile(output.filePath, TextFile.WriteOnly);
                file.write(content);
                file.close();
            }
            return [cmd];
        }
    }
}

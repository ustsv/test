import qbs 1.0

Project {
    qbsSearchPaths: "qbs";
    minimumQbsVersion: "1.11.0";

    references: [   "test_project.qbs"  ]
}
